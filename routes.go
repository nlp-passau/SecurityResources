package main

import (
	"net/http"
	"gitlab.com/nlp-passau/SecurityResources/cve"
)

type Route struct {
	Name string
	Method string
	Pattern string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"HelpIndex",
		"GET",
		"/",
		cve.HelpIndex,
	},
	Route{
		"GetVulnerability",
		"GET",
		"/vul/{vendor}/{product}/{versions}",
		cve.GetVulnerability,
	},
}