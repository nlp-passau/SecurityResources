package cve

import (
	"net/url"
)

type Vulnerability struct {
	Id string `json:"id"`
	Assigner string `json:"assigner"`
	Problem []struct {
		Desc []Description `json:"desc"`
	} `json:"problem"`
	Refs []url.URL `json:"refs"`
	Desc []Description `json:"desc"`
	Impact struct { //using base matric v3 only.
		Version string `json:"version"`
		VectorString string `json:"vectorString"`
		AttackVector string `json:"attackVector"`
		Complexity string `json:"complexity"`
		PrivilegesRequired string `json:"privilegesRequired"`
		UserInteraction string `json:"userInteraction"`
		Scope string `json:"sScope"`
		ConfidentialityImpact string `json:"confidentialityImpact"`
		IntegrityImpact string `json:"integrityImpact"`
		AvailabilityImpact string `json:"availabilityImpact"`
		BaseScore float64 `json:"baseScore"`
		BaseSeverity string `json:"baseSeverity"`
		ExploitabilityScore float64 `json:"exploitabilityScore"`
		ImpactScore float64 `json:"impactScore"`
	} `json:"impact"`
	Published string `json:"published"`
	LastModified string `json:"lastModified"`
	Products []struct {
		Product Product `json:"product"`
		Versions []string `json:"versions"`
	} `json:"products"`
}

type Description struct {
	Lang  string `json:"lang"`
	Value string `json:"value"`
}

type Product struct {
	Vendor string `json:"vendor"`
	Name string `json:"name"`
}

