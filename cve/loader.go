package cve

import (
	"log"
	"io/ioutil"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"path/filepath"
)

type MongoDataBase struct {
	Address string
	Name    string
}

func (m *MongoDataBase) loadAll(d string) bool {
	files, err := ioutil.ReadDir(d)
	if err != nil {
		log.Fatal(err)
		return false
	}

	for _, f := range files {
		i := NewIterator(filepath.Join(d, f.Name()))
		if !m.load(i) {
			return false
		}
	}
	return true
}

func (m *MongoDataBase) getCollection() (*mgo.Session, *mgo.Collection) {
	session, err := mgo.Dial(m.Address)
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	return session, session.DB(m.Name).C("vulnerability")
}

func (m *MongoDataBase) load(i *MainCVEIterator) bool {
	s, c := m.getCollection()
	defer s.Close()

	for i.Next() {
		v := i.CurrentValue
		err := c.Insert(v)
		if err != nil {
			log.Fatal(err)
		}
	}

	return true
}

func (m *MongoDataBase) GetVulnerability(vendor string, product string, version string) *[]Vulnerability {

	var q []bson.M

	if vendor != "_"{
		q = append(q, bson.M{"products.product.vendor" : vendor})
	}
	if product != "_" {
		q = append(q, bson.M{"products.product.name" : product})
	}
	if version != "_" {
		q = append(q, bson.M{"products.versions" : version})
	}

	if len(q) == 0 {
		log.Fatal()
	}

	s, c := m.getCollection()
	defer s.Close()

	var vs []Vulnerability
	err := c.Find(bson.M{"$and" : q}).Sort("-start").All(&vs)
	if err != nil {
		panic(err)
	}

	return &vs
}

