package cve

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"encoding/json"
)
var mongo = MongoDataBase{"localhost", "security"}

func HelpIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n\n - to search for vulnerabilities:\n")
	fmt.Fprint(w," GET /vul/{vendor}/{product}/{versions}\n\n")
	fmt.Fprint(w,"    Examples of use:\n")
	fmt.Fprint(w,"    - GET /vul/oracle/java/1.8\n")
	fmt.Fprint(w,"    - GET /vul/microsoft/-/-")
}

func GetVulnerability(w http.ResponseWriter, r *http.Request) {
	v := mux.Vars(r)
	vendor := v["vendor"]
	product := v["product"]
	versions := v["versions"]

	vuls := mongo.GetVulnerability(vendor, product, versions)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err:= json.NewEncoder(w).Encode(vuls); err !=nil {
		panic(err)
	}
}

