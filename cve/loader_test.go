package cve

import (
	"testing"
)

func TestLoadAll(t *testing.T) {
	//db := MongoDataBase{"132.231.91.193", "security"}
	db := MongoDataBase{"localhost", "security"}
	//db.loadAll("/home/juliano/Documents/CS-AWARE")

	vuls := db.GetVulnerability("microsoft", "office", "")
	if len(*vuls) == 0 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", 20, 10)
	}
}
