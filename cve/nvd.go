package cve

import (
	"net/url"
	"log"
	"io/ioutil"
	"fmt"
	"os"
	"encoding/json"
)

type MainCVE struct {
	CVEDataType         string `json:"CVE_data_type"`
	CVEDataFormat       string `json:"CVE_data_format"`
	CVEDataVersion      string `json:"CVE_data_version"`
	CVEDataNumberOfCVEs string `json:"CVE_data_numberOfCVEs"`
	CVEDataTimestamp    string `json:"CVE_data_timestamp"`
	CVEItems []struct {
		CVE struct {
			DataType    string `json:"data_type"`
			DataFormat  string `json:"data_format"`
			DataVersion string `json:"data_version"`
			CVEDataMeta struct {
				ID       string `json:"ID"`
				ASSIGNER string `json:"ASSIGNER"`
			} `json:"CVE_data_meta"`
			Affects struct {
				Vendor struct {
					VendorData []struct {
						VendorName string `json:"vendor_name"`
						Product struct {
							ProductData []struct {
								ProductName string `json:"product_name"`
								Version struct {
									VersionData []struct {
										VersionValue string `json:"version_value"`
									} `json:"version_data"`
								} `json:"version"`
							} `json:"product_data"`
						} `json:"product"`
					} `json:"vendor_data"`
				} `json:"vendor"`
			} `json:"affects"`
			Problemtype struct {
				ProblemtypeData []struct {
					Description []Description `json:"description"`
				} `json:"problemtype_data"`
			} `json:"problemtype"`
			References struct {
				ReferenceData []struct {
					URL string `json:"url"`
				} `json:"reference_data"`
			} `json:"references"`
			Description struct {
				DescriptionData []struct {
					Lang  string `json:"lang"`
					Value string `json:"value"`
				} `json:"description_data"`
			} `json:"description"`
		} `json:"cve"`
		Configurations struct {
			CVEDataVersion string `json:"CVE_data_version"`
			Nodes []struct {
				Operator string `json:"operator"`
				Cpe []struct {
					Vulnerable bool   `json:"vulnerable"`
					Cpe22URI   string `json:"cpe22Uri"`
					Cpe23URI   string `json:"cpe23Uri"`
				} `json:"cpe"`
			} `json:"nodes"`
		} `json:"configurations"`
		Impact struct {
			BaseMetricV3 struct {
				CvssV3 struct {
					Version               string  `json:"version"`
					VectorString          string  `json:"vectorString"`
					AttackVector          string  `json:"attackVector"`
					AttackComplexity      string  `json:"attackComplexity"`
					PrivilegesRequired    string  `json:"privilegesRequired"`
					UserInteraction       string  `json:"userInteraction"`
					Scope                 string  `json:"scope"`
					ConfidentialityImpact string  `json:"confidentialityImpact"`
					IntegrityImpact       string  `json:"integrityImpact"`
					AvailabilityImpact    string  `json:"availabilityImpact"`
					BaseScore             float64 `json:"baseScore"`
					BaseSeverity          string  `json:"baseSeverity"`
				} `json:"cvssV3"`
				ExploitabilityScore float64 `json:"exploitabilityScore"`
				ImpactScore         float64 `json:"impactScore"`
			} `json:"baseMetricV3"`
			BaseMetricV2 struct {
				CvssV2 struct {
					Version               string  `json:"version"`
					VectorString          string  `json:"vectorString"`
					AccessVector          string  `json:"accessVector"`
					AccessComplexity      string  `json:"accessComplexity"`
					Authentication        string  `json:"authentication"`
					ConfidentialityImpact string  `json:"confidentialityImpact"`
					IntegrityImpact       string  `json:"integrityImpact"`
					AvailabilityImpact    string  `json:"availabilityImpact"`
					BaseScore             float64 `json:"baseScore"`
				} `json:"cvssV2"`
				Severity                string  `json:"severity"`
				ExploitabilityScore     float64 `json:"exploitabilityScore"`
				ImpactScore             float64 `json:"impactScore"`
				ObtainAllPrivilege      bool    `json:"obtainAllPrivilege"`
				ObtainUserPrivilege     bool    `json:"obtainUserPrivilege"`
				ObtainOtherPrivilege    bool    `json:"obtainOtherPrivilege"`
				UserInteractionRequired bool    `json:"userInteractionRequired"`
			} `json:"baseMetricV2"`
		} `json:"impact"`
		PublishedDate    string `json:"publishedDate"`
		LastModifiedDate string `json:"lastModifiedDate"`
	} `json:"CVE_Items"`
}

type MainCVEIterator struct {
	MainCVE      *MainCVE
	Index        int
	CurrentValue *Vulnerability
	Error        error
}

func NewIterator(f string) *MainCVEIterator {
	raw, err := ioutil.ReadFile(f)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var c MainCVE
	json.Unmarshal(raw, &c)
	var v *Vulnerability

	return &MainCVEIterator{&c, 0, v, nil}
}

func (s *MainCVEIterator) Next() bool {
	if s.Index < len(s.MainCVE.CVEItems) {
		c := s.MainCVE.CVEItems[s.Index]
		s.Index++

		v := Vulnerability{}
		v.Id = c.CVE.CVEDataMeta.ID
		v.Assigner = c.CVE.CVEDataMeta.ASSIGNER

		for _, p := range c.CVE.Problemtype.ProblemtypeData {
			var ds []Description

			for _, d := range p.Description {
				ds = append(ds, d)
			}

			v.Problem = append(v.Problem, struct{ Desc []Description `json:"desc"`} {ds})
		}

		for _, r := range c.CVE.References.ReferenceData {
			u, err := url.Parse(r.URL)
			if err != nil {
				log.Fatal(err)
			}
			v.Refs = append(v.Refs, *u)
		}

		for _, d := range c.CVE.Description.DescriptionData {
			v.Desc = append(v.Desc, Description{d.Lang, d.Value})
		}

		v.Impact.Version = c.Impact.BaseMetricV3.CvssV3.Version
		v.Impact.VectorString = c.Impact.BaseMetricV3.CvssV3.VectorString
		v.Impact.AttackVector = c.Impact.BaseMetricV3.CvssV3.AttackVector
		v.Impact.Complexity = c.Impact.BaseMetricV3.CvssV3.AttackComplexity
		v.Impact.PrivilegesRequired = c.Impact.BaseMetricV3.CvssV3.PrivilegesRequired
		v.Impact.UserInteraction = c.Impact.BaseMetricV3.CvssV3.UserInteraction
		v.Impact.Scope = c.Impact.BaseMetricV3.CvssV3.Scope
		v.Impact.ConfidentialityImpact = c.Impact.BaseMetricV3.CvssV3.ConfidentialityImpact
		v.Impact.IntegrityImpact = c.Impact.BaseMetricV3.CvssV3.IntegrityImpact
		v.Impact.AvailabilityImpact = c.Impact.BaseMetricV3.CvssV3.AvailabilityImpact
		v.Impact.BaseScore = c.Impact.BaseMetricV3.CvssV3.BaseScore
		v.Impact.BaseSeverity = c.Impact.BaseMetricV3.CvssV3.BaseSeverity
		v.Impact.ExploitabilityScore = c.Impact.BaseMetricV3.ExploitabilityScore
		v.Impact.ImpactScore = c.Impact.BaseMetricV3.ImpactScore

		v.Published = c.PublishedDate
		v.LastModified = c.LastModifiedDate

		for _, vd := range c.CVE.Affects.Vendor.VendorData {
			for _, pd := range vd.Product.ProductData {
				var vrs []string
				for _, vs := range pd.Version.VersionData {
					vrs = append(vrs, vs.VersionValue)
				}
				p := struct {Product Product `json:"product"`
				Versions []string `json:"versions"`}{Product{vd.VendorName, pd.ProductName}, vrs}
				v.Products = append(v.Products, p)
			}
		}

		s.CurrentValue = &v
		return true
	}

	return false
}
