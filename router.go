package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

func NewRouter() *mux.Router {
	tr := mux.NewRouter().StrictSlash(true)
	for _, r := range routes {
		var h http.Handler
		h = r.HandlerFunc
		h = Logger(h, r.Name)
		tr.Methods(r.Method).Path(r.Pattern).Name(r.Name).Handler(h)
	}

	return tr
}
